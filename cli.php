#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Pimple\Container;
use Sam\Services\Metric as MetricService;
use Sam\Services\Unit as UnitService;
use Symfony\Component\Console\Application;

$application = new Application();

$capsule = new Capsule;

$container = new Container();
$container->register(new UnitService());
$container->register(new MetricService());

$capsule->addConnection(array(
    'driver' => 'mysql',
    'host' => 'mysam',
    'database' => 'sam',
    'username' => 'sam',
    'password' => 'sam',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
));
$capsule->setAsGlobal();
$capsule->bootEloquent();

$application->addCommands([
    // Essential commands
    new Sam\Command\IngestCommand($container)
]);

$application->run();