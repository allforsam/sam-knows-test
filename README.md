# Code test for Sam Knows


I am basing my code test on two tables. Units and metrics.

I am asuming the load will be low. If we had to deal with trillions of data,
we would have to partition the `metrics` table. I'd suggest by a mod or any algorithm based on the `unit_id` or even a combination 
of `unit_id` and `action`. Something like `metrics_001_download` the idea is that once you partition you 
have to denormalize your database and likely will be able to query by `date` or `unit_id`, removing the action from the query.
Assuming that the data will not change once is in the table, a combination of cache layer or archiving strategy would apply 
in order to keep the tables as much smallers as possible. I would also think on any date based partition strategy as we will
likely collect tons of data daily.

## Database schema

###Units table

    CREATE TABLE `units` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `name` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

###Metrics table
    
    CREATE TABLE `metrics` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `action` varchar(50) NOT NULL DEFAULT '',
      `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `value` int(11) NOT NULL,
      `unit_id` int(11) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `action_unit_index` (`action`,`unit_id`),
      KEY `date_index` (`timestamp`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  