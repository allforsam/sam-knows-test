<?php

namespace Sam\Command;

use Sam\Repository\UnitsRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class IngestCommand
 * @package Sam\Command
 */
class IngestCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {

        $this->setName('Ingest');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UnitsRepository $userRepository */
        $unitsRepository = $this->container['UnitsRepository'];
        foreach ($unitsRepository->getAll() as $unit) {
            if (!empty($unit->metrics()->first())) var_export($unit->metrics()->get()->toArray());
        }
    }
}