<?php

namespace Sam\Command;

use Pimple\Container;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

/**
 * Class ContainerAwareCommand
 * @package Sam\Command
 */
abstract class ContainerAwareCommand extends SymfonyCommand
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Container $container
     * @param string|null $name
     */
    public function __construct(Container $container, $name = null)
    {
        $this->container = $container;
        parent::__construct($name);
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    protected function get($id)
    {
        return $this->container[$id];
    }
}