<?php
/**
 * Copyright (c) 2017. This file belongs to Misericordia di "Torre del lago Puccini"
 */
namespace Sam\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sam\Repository\UnitsRepository;

/**
 * Class Unit
 * @package Sam\Services
 */
class Unit implements ServiceProviderInterface
{
    /**
     * @param Container $container
     */
    public function register(Container $container)
    {
        /**
         * @param Container $container
         * @return UnitsRepository
         */
        $container['UnitsRepository'] = function (Container $container) {
            return new UnitsRepository('\Sam\Model\Unit');
        };
    }
}