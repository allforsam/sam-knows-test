<?php
/**
 * Copyright (c) 2017. This file belongs to Misericordia di "Torre del lago Puccini"
 */
namespace Sam\Services;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sam\Repository\MetricsRepository;

/**
 * Class Unit
 * @package Sam\Services
 */
class Metric implements ServiceProviderInterface
{
    /**
     * @param Container $container
     */
    public function register(Container $container)
    {
        /**
         * @param Container $container
         * @return MetricsRepository
         */
        $container['MetricRepository'] = function (Container $container) {
            return new MetricsRepository('\Sam\Model\Metric');
        };
    }
}