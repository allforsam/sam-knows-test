<?php

namespace Sam\Model;


use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    /**
     * string collection name
     */
    protected $table = 'units';
    protected $fillable = ['name'];

    public function metrics()
    {
        return $this->hasMany('Sam\Model\Metric', 'unit_id');
    }

}