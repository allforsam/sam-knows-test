<?php

namespace Sam\Model;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    /**
     * string collection name
     */
    protected $table = 'metrics';
    protected $fillable = [
        'action',
        'value',
        'unit_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'timestamp'
    ];

    public function unit()
    {
        return $this->belongsTo(
            'Sam\Model\Unit',
            'unit_id'
        );
    }
}
